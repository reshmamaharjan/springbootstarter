package com.springboot.model;

/**
 * @Author Reshma
 * @Date 7/15/2018
 * @Month 07
 **/
public class Employee {
    public int id;
    public String name;
    public String address;

    public Employee(){

    }
    public Employee(int id){
        this.id = id;
    }

    public Employee(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
