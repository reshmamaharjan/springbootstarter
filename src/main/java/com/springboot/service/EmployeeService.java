package com.springboot.service;

import com.springboot.controller.HomeController;
import com.springboot.model.Employee;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author Reshma
 * @Date 7/15/2018
 * @Month 07
 **/
@Service
public class EmployeeService {
    List<Employee>  employees=new ArrayList<>(
            Arrays.asList(new Employee(1,"Asha","Baneshwor"),
                    new Employee(2,"Reshma","Samakhushi"),
                    new Employee(3,"Milon","Baneshwor"))
    );


    public List<Employee> getEmployees(){
        return employees;
    }

    public Employee getEmployeeById(int id){
    return employees.stream().filter(e -> e.getId() == id).findFirst().get();
    }

    public void addEmployee(Employee employee){
        employees.add(employee);
    }

    public void updateEmployee(int id, Employee employee) {
        for(int i=0;i<employees.size();i++){
            Employee e=employees.get(i);
            if(e.getId()==id){
                employees.set(i,employee);
                return;
            }
        }
    }

    public void deleteEmployee(int id) {
        employees.removeIf(t->t.getId()==id);
    }
}
