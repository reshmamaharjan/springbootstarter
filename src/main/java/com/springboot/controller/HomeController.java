package com.springboot.controller;

import com.springboot.model.Employee;
import com.springboot.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author Reshma
 * @Date 7/15/2018
 * @Month 07
 **/
@RestController
public class HomeController {

    @Autowired
    private EmployeeService employeeService;
    @RequestMapping("/employees")
    public List<Employee> getEmployees(){
        return  employeeService.getEmployees();
    }

    @RequestMapping("/employees/{id}")
    public Employee getEmployeeById(@PathVariable("id") int eid){
        return  employeeService.getEmployeeById(eid);
    }

    @RequestMapping(value = "/employees",method = RequestMethod.POST)
    public void addEmployee(@RequestBody Employee employee){
        employeeService.addEmployee(employee);

    }

    @RequestMapping(value = "/employees/{id}",method = RequestMethod.PUT)
    public void updateEmployee(@PathVariable("id") int id, @RequestBody Employee employee){
        employeeService.updateEmployee(id,employee);
    }

    @RequestMapping(value = "/employees/{id}",method = RequestMethod.DELETE)
    public void deleteEmployee(@PathVariable int id){
        employeeService.deleteEmployee(id);
    }

}
